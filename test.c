/*
 * https://gitlab.com/bztsrc/minix3fs
 *
 * Copyright (C) 2023 bzt (MIT license)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL ANY
 * DEVELOPER OR DISTRIBUTOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @brief simple test for mfs.h
 *
 * Compile with:
 *   gcc test.c -o test
 *
 * Run with:
 *   ./test (mfs image file) (path)
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MFS_IMPLEMENTATION
#include "mfs.h"

FILE *f = NULL;

/**
 * Emulate sector reads
 */
void loadsec(uint32_t sec, uint32_t cnt, void *dst) { fseek(f, sec << 9, SEEK_SET); fread(dst, cnt, 512, f); }

/**
 * Main function
 */
int main(int argc, char **argv)
{
    int i, j;
    uint64_t offs, len;
    uint8_t *data;

    /* check arguments */
    if(argc < 3) {
        printf(" %s <mfsimgfile> <path>\r\n\r\n", argv[0]);
        return 1;
    }

    /* open file system image file */
    f = fopen(argv[1], "rb");
    if(!f) { fprintf(stderr, "unable to open image\r\n"); return 2; }

    /* test open / read / close functions */
    len = mfs_open(argv[2]);
    printf("mfs_open(%s) = %ld (inode %d)\r\n", argv[2], len, mfs_inode);
    if(len == -1UL) { fprintf(stderr, "Minix3 File System not found in image\r\n"); fclose(f); return 3; }
    if(!len) { fprintf(stderr, "unable to locate path '%s' in image\r\n", argv[2]); fclose(f); return 3; }

    data = malloc(len);
    if(!data) { fprintf(stderr, "unable to allocate memory\r\n"); fclose(f); return 4; }
    offs = len;

    len = mfs_read(0, offs, data);
    printf("mfs_read(0,%ld,ptr) = %ld\r\n", offs, len);

    /* hexdump file contents */
    for(offs = 0, j = 16; offs < len; offs += j, data += j) {
        j = offs + 16 < len ? 16 : len - offs;
        printf("%06lx: ", offs);
        for(i = 0; i < 16; i++) if(i < j) printf("%02x ", data[i]); else printf("   ");
        printf(" ");
        for(i = 0; i < j; i++) printf("%c", data[i] >= 32 && data[i] < 127 ? data[i] : '.');
        printf("\n");
    }

    mfs_close();
    printf("mfs_close()\r\n");

    /* teardown */
    fclose(f);
    return 0;
}
